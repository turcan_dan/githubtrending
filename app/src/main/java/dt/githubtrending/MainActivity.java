package dt.githubtrending;


import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends ListActivity {

    private ProgressDialog pDialog;

    // URL to get contacts JSON
    static String url = "https://api.github.com/search/repositories?sort=stars&order=desc&q=created:>2016-07-04";

    // JSON Node names
    static final String TAG_REPOS = "items";
    static final String TAG_NAME = "name";
    static final String TAG_STARSTODAY = "stargazers_count";
    static final String TAG_PHOTO = "avatar_url";

    private GetRepositories getRepos;


    // contacts JSONArray
    JSONArray repositories = null;

    // Hashmap for ListView
    ArrayList<Repository> repositoryList;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //PooExamples.run();

        repositoryList = new ArrayList<Repository>();

        ListView lv = getListView();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String name = ((TextView) view.findViewById(R.id.name))
                        .getText().toString();
                String star = ((TextView) view.findViewById(R.id.stars))
                        .getText().toString();
                //String photo = ((TextView)view.findViewById(R.id.photo)).get

                String photo = null;

                for (Repository repos : repositoryList
                        ) {
                    if (repos.getName() == name) {
                        photo = repos.getPhoto();
                    }
                }

                // Starting single contact activity
                Intent in = new Intent(getApplicationContext(),
                        SingleRepositoryActivity.class);
                in.putExtra(TAG_NAME, name);
                in.putExtra(TAG_STARSTODAY, star.toString());
                in.putExtra(TAG_PHOTO, photo);
                startActivity(in);

            }
        });


        // Calling async task to get json

        getRepos = new GetRepositories();

        getRepos.execute();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onDestroy() {

        if (getRepos != null)
            getRepos.cancel(true);

        super.onDestroy();

    }


    private class GetRepositories extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            // Making a request to url and getting response
            String jsonStr = HttpUtils.getPageContent(url);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    repositories = jsonObj.getJSONArray(TAG_REPOS);

                    // looping through All Repositories
                    for (int i = 0; i < repositories.length(); i++) {
                        JSONObject c = repositories.getJSONObject(i);

                        String name = c.getString(TAG_NAME);
                        Integer stars = c.getInt(TAG_STARSTODAY);
                        String photo = c.getJSONObject("owner").getString(TAG_PHOTO);

                        // tmp hashmap for single repository
                        Repository contact = new Repository(name, stars, photo);

                        // adding repository to repositoryList
                        repositoryList.add(contact);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            ListView lv = getListView();

            ListAdapt customAdapter = new ListAdapt(MainActivity.this, R.layout.list_item, repositoryList);

            lv.setAdapter(customAdapter);

        }

    }

}
