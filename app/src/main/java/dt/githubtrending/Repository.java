package dt.githubtrending;


public final class Repository {

    private String name;
    private Integer stars;
    private String photo;

    public Repository(String name, Integer stars, String photo) {
        this.name = name;
        this.stars = stars;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public Integer getStars() {
        return stars;
    }

    public String getPhoto() {
        return photo;
    }
}
