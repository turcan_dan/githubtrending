package dt.githubtrending;


import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class SingleRepositoryActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_item);

        String name;
        String stars;
        String photo = null;
        Bundle extras = getIntent().getExtras();


        if (savedInstanceState == null) {

            if (extras == null) {
                name = null;
                stars = null;
                photo = null;
            } else {
                name = extras.getString(MainActivity.TAG_NAME);
                stars = extras.getString(MainActivity.TAG_STARSTODAY);
                photo = extras.getString(MainActivity.TAG_PHOTO);
            }
        } else {
            name = (String) savedInstanceState.getSerializable(MainActivity.TAG_NAME);
            stars = (String) savedInstanceState.getSerializable(MainActivity.TAG_STARSTODAY);
            photo = (String) savedInstanceState.getSerializable(MainActivity.TAG_PHOTO);
        }

        TextView tt1 = (TextView) findViewById(R.id.name);
        TextView tt2 = (TextView) findViewById(R.id.stars);
        ImageView tt3 = (ImageView) findViewById(R.id.photo);

        tt1.setText(name);
        tt2.setText(stars);
        Picasso.with(getApplicationContext()).load(photo).into(tt3);
    }
}
