package dt.githubtrending;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ListAdapt extends ArrayAdapter<Repository> {

    private Context context;

    public ListAdapt(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapt(Context context, int resource, List<Repository> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_item, null);
        }
        Repository p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.name);
            TextView tt2 = (TextView) v.findViewById(R.id.stars);
            ImageView tt3 = (ImageView) v.findViewById(R.id.photo);

            if (tt1 != null) {
                tt1.setText(p.getName());
            }
            if (tt2 != null) {
                tt2.setText(p.getStars().toString());
            }
            if (tt3 != null) {
                Picasso.with(context).load(p.getPhoto()).into(tt3);
            }
        }
        return v;
    }
}